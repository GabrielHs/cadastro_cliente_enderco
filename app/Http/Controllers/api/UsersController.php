<?php

namespace App\Http\Controllers\api;

use App\Acesso;
use App\Endereco;
use App\Http\Controllers\Controller;
use App\repositories\User\Helper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{


    public function login_cliente(Request $request)
    {

        $credentials = $request->only(['email', 'password']);


        $emailValido  = Helper::isValidEmail($credentials['email']);


        if (!$emailValido) {
            return ['error' => 'Por favor, informe um e-mail válido'];
        }

        $acessoCliente = Acesso::where('email', $credentials['email'])->first();

        if (!$acessoCliente) {
            return ['error' => 'E-mail não encontrado'];
        }
        $user = User::where('id', $acessoCliente->user_id)->first();

        if (!$user) {
            return ['error' => 'Usuário não encontrado'];
        }


        $user->load('acessos');

        if (Hash::check($credentials['password'], $acessoCliente->password)) {
            $token = JWTAuth::fromUser($user);
            $user['jwt'] = $token;

            return ['ok' => $user];
        }

        return ['error' => 'Sua senha esta incorreta'];
    }

    /**
     * Listagem dos users cadastrados
     */
    public function index()
    {

        $somente = request('somente');

        $userLogado = Auth::user();

        $users = User::where('id', '<>', $userLogado->id);

        $users->with('acessos');
        $users->with('enderecos');

        if ($somente) {
            $users->where('name', $somente);
            $users->orWhere('cpf', $somente);
            $users->orWhere('name', 'like', "%$somente%");
            $users->orWhereHas('acessos', function ($q) use ($somente) {
                $q->where('email', $somente);
            });
        }

        // if ($somente) {

        //     $users = User::where('name', $somente)
        //         ->with('acessos')
        //         ->with('enderecos')
        //         ->orWhere('cpf', $somente)
        //         ->orWhere('name', 'like', "%$somente%")
        //         ->orWhereHas('acessos', function ($q) use ($somente) {
        //             $q->where('email', $somente);
        //         })
        //         ->paginate();
        //     return ['users' => $users,];
        // } else {

        //     $users = User::paginate();
        //     $users->load('acessos');

        //     $users->load('enderecos');

        //     return ['users' => $users,];
        // }

        return ['users' => $users->paginate(10)];
    }



    public function store(Request $request)
    {

        try {

            $exitUser = User::where('name', $request->name)->first();

            if ($exitUser) {
                return ['erro' => 'desculpe, já esta cadastrado na nossa base'];
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:users|max:50',
                'nascimento' => 'required',
                'email' => 'required',
                'cpf' => 'required',
                'password' => 'required',

            ]);

            if ($validator->fails()) {

                return ['erro' => $validator->errors()];
            }

            $isCpf = Helper::validaCPF($request->cpf);


            if (!$isCpf) {
                return ['error' => 'Por favor, informe um cpf válido'];
            }

            $emailValido  = Helper::isValidEmail($request->email);

            if (!$emailValido) {
                return ['error' => 'Por favor, informe um e-mail válido'];
            }

            $data  = $request->all();

            $user = new User;

            $user->name = $data['name'];
            $date = date_create($data['nascimento']);
            $user->nascimento = date_format($date, 'Y-m-d H:i:s');;

            $cpfValido  = Helper::limpaCPF_CNPJ($data['cpf']);

            $user->cpf = $cpfValido;

            $user->save();

            $acesso = new Acesso;
            $acesso->email = $data['email'];
            $acesso->password =  Hash::make($data['password']);


            $user->acessos()->save($acesso);

            return ['ok' => 'usuário criado com sucesso'];
        } catch (\Exception $e) {

            return ['erro' => $e->getMessage()];
        }
    }


    public function update(Request $request, $id)
    {
        if (!$id) {
            return ['erro' => 'não informado usuario'];
        }

        try {
            $user = User::find($id);
            // $user = User::with($id);

            if (!$user) {
                return ['erro' => 'desculpe, usúario não na nossa base'];
            }


            $user->name = $request->name;

            $user->nascimento = $request->nascimento;

            $isCpf = Helper::validaCPF($request->cpf);


            if (!$isCpf) {
                return ['error' => 'Por favor, informe um cpf válido'];
            }

            $cpfFormatado  = Helper::limpaCPF_CNPJ($request->cpf);
            $user->cpf = $cpfFormatado;

            $user->save();


            $endereco = new Endereco;

            $endereco->logadouro = $request->logadouro;

            $endereco->numero_imovel = $request->numero_imovel;

            $endereco->complemento = $request->complemento ? $request->complemento : "Não informado";

            $endereco->bairro = $request->bairro;

            $endereco->cidade = $request->cidade;

            $endereco->estado = $request->estado;

            $endereco->latitude = $request->latitude;

            $endereco->longitude = $request->logitude;


            $user->enderecos()->save($endereco);

            return ['ok' => 'Atualizado cadastro'];
        } catch (\Exception $e) {

            return ['erro' => $e->getMessage()];
        }
    }



    public function destroy($id)
    {
        if (!$id) {
            return ['erro' => 'não informado usuario'];
        }


        $user = User::find($id);

        $userLogado = Auth::user();

        if ($user->id == $userLogado->id) {

            abort(400, 'desculpe não possivel excluirp');
        }

        if (!$user) {
            return ['erro' => 'desculpe, usúario não na nossa base'];
        }

        $user->delete();

        return ['ok' => 'usuário excluido com sucesso'];
    }

    public function show($id)
    {
        if (!$id) {
            return ['erro' => 'não informado usuario'];
        }

        $user = User::find($id);
        $user->load('acessos');
        $user->load('enderecos');



        return ['dados' => $user];
    }
}
