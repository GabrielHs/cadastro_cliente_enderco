<?php

namespace App\Http\Controllers\api;

use App\Endereco;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class EnderecosController extends Controller
{
    public function create(Request $request)
    {
        // if (!$id) {
        //     return ['erro' => 'não informado usuario'];
        // }
        try {
            $validator = Validator::make($request->all(), [
                'logadouro' => 'required||max:150',
                'numero_imovel' => 'required',
                'bairro' => 'required',
                'cidade' => 'required',
                'estado' => 'required',
                'latitude' => 'required',
                'logitude' => 'required',
            ]);

            if ($validator->fails()) {

                return ['erro' => $validator->errors()];
            }

            $user = Auth::user();

            $endereco = new Endereco;
            
            $endereco->logadouro = $request->logadouro;
            $endereco->numero_imovel = $request->numero_imovel;
            $endereco->complemento = $request->complemento ? $request->complemento : "Não informado";
            $endereco->bairro = $request->bairro;
            $endereco->cidade = $request->cidade;
            $endereco->estado = $request->estado;
            $endereco->latitude = $request->latitude;
            $endereco->longitude = $request->logitude;

            $user->enderecos()->save($endereco);
           
            return ['ok' => 'endereco criado',];

        } catch (\Exception $e) {

            return ['erro' => $e->getMessage()];
        }
    }
}
