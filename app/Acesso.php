<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acesso extends Model
{

    protected $fillable = ['email', 'password'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
