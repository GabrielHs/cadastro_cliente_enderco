
## Cadastro de cliente e endereço com Laravel

## Pre-requisito

- Após clonar o projeto, rodar comandos abaixo na raiz do projeto,
``
    composer install
    composer update
``

- Renomei o arquivo de (.env.Example) para (.env)
- Crie o banco de dados cadastro_progeo
- No arquivo .env passe credenciais do banco usuario e senha  

- Rode comando 
``
    php artisan key:generate
`` 

- Rode comando, onde criará todas as tableas p/banco
 ``
    php artisan migrate
 `` 


## App (frontend)
Clonar [repositorio](https://bitbucket.org/GabrielHs/app-cadastro-cliente_endereco/src).



