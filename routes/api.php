<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('clientes/create', 'api\UsersController@store');

Route::post('clientes/login', 'api\UsersController@login_cliente');



Route::middleware('jwt.auth')->group(static function () {
    Route::get('clientes', 'api\UsersController@index');
    Route::get('clientes/{id}', 'api\UsersController@show');
    Route::put('clientes/{id}', 'api\UsersController@update');
    Route::delete('clientes/{id}', 'api\UsersController@destroy');

    
    Route::post('endereco/create', 'api\EnderecosController@create');

});


